<?php /*$this->nombre_empresa = 'Favalu';
	  $this->metodo = 'Metodo';
	  $this->controller = 'Controlador';*/

	$imagenes_web = array(
		'logo_cabecera' => 'assets/img/cti_logo.png', 
		'logo_pie' => 'assets/img/logo.png',
		'fondo' => array( 'inicio' => 'images/unsm.jpg' , 
						  'nosotros' => 'images/unsm-tarapoto-cpu-postulantes.jpg' , 
						  'contacto' => 'images/unsm-computo.jpg'  ),
	);

	$informacion_contacto = array(
		'correo' =>  array( 'ctiunsm@gmail.com' ),
		'telefono' => array( '042 -480142',  '955 941 992' , '944 929 637' ),
		'telefono_principal' => '955 941 992',
		'fanpage' => array( 'www.facebook.com/cti.unsm'),
		'direccion' => array( 'Jr. Orellana 575 - Tarapoto'),
	);

  	$modulos_web  = array(  array('Inicio', 'index.php'),
					array('Nosotros', 'nosotros.php'),
					array('Talleres', 'talleres.php'),
					array('Ponentes', 'ponentes.php'),
					array('Aliados', 'Aliados.php'),
					array('Contacto', 'contacto.php')
				);

  	$sliders_web  = array(  
							array('img' => 'assets/img/carrousel/carrousel_1.jpg',
								  'texto' => '<span> II  SEMINARIO INTERNACIONAL <br> "Aplicabilidad de las TIC\'S en la gestión del conocimiento"</span> ' ),
							array('img' => 'assets/img/carrousel/carrousel_2.jpg',
								  'texto' => "")
					);

?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
  	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><?php echo $this->pagina_web; ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/styles/bootstrap4/bootstrap.min.css') ?>" >
	<link href="<?php echo base_url('assets/themes/course/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') ?>" type="text/css">
	

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/plugins/OwlCarousel2-2.2.1/owl.carousel.css') ?>" >
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/plugins/OwlCarousel2-2.2.1/owl.theme.default.css') ?>" >
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/plugins/OwlCarousel2-2.2.1/animate.css') ?>" >
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/styles/main_styles.css') ?>" >
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/styles/responsive.css') ?>" >

	<!-- <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/styles/courses_styles.css') ?>" >
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/styles/ourses_responsive.css') ?>"> -->

	<link rel='shortcut icon' type='image/png' href="<?php echo base_url( $this->icono_pagina_web )?>"/>

	<!-- <script src="<?php echo base_url('assets/themes/course/js/jquery-3.2.1.min.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/styles/bootstrap4/popper.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/styles/bootstrap4/bootstrap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/plugins/greensock/TweenMax.min.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/plugins/greensock/TimelineMax.min.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/plugins/scrollmagic/ScrollMagic.min.js')?>"></script>

	<script src="<?php echo base_url('assets/themes/course/plugins/greensock/animation.gsap.min.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/plugins/greensock/ScrollToPlugin.min.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/plugins/OwlCarousel2-2.2.1/owl.carousel.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/plugins/scrollTo/jquery.scrollTo.min.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/plugins/easing/easing.js')?>"></script>
	<script src="<?php echo base_url('assets/themes/course/js/custom.js')?>"></script> -->
	
</head>
<body>


	<div class="super_container">

		<!-- Header -->

		<header class="header d-flex flex-row">
			<div class="header_content d-flex flex-row align-items-center">
				<!-- Logo -->
				<div class="logo_container">
					<div class="logo">
						<img src="<?= $imagenes_web['logo_cabecera'] ?>" alt="logo_principal" style="width: 120px;height: 54px">
						<!--span>course</span-->
					</div>
				</div>

				<!-- Main Navigation -->
				<nav class="main_nav_container">
					<div class="main_nav">
						<ul class="main_nav_list">

							<? foreach ($modulos_web as $key => $val) : ?>
								<li class='main_nav_item'><a href='<?= $val[1] ?>'><?= $val[0] ?></a></li>
							<? endforeach; ?>
							
						</ul>
					</div>
				</nav>
			</div>
			<div class="header_side d-flex flex-row justify-content-center align-items-center">
				<img src="assets/img/iconos/phone-call.svg" alt="">
				<span><?= $informacion_contacto['telefono_principal'] ?></span>
			</div>

			<!-- Hamburger -->
			<div class="hamburger_container">
				<i class="fas fa-bars trans_200"></i>
			</div>

		</header>
		
		<!-- Menu -->
		<div class="menu_container menu_mm">

			<!-- Menu Close Button -->
			<div class="menu_close_container">
				<div class="menu_close"></div>
			</div>

			<!-- Menu Items -->
			<div class="menu_inner menu_mm">
				<div class="menu menu_mm">
					<ul class="menu_list menu_mm">

						<? foreach ($modulos_web as $key => $val) : ?>
							<li class='menu_item menu_mm'><a href='<?= $val[1] ?>'><?= $val[0] ?></a></li>
						<? endforeach; ?>
					</ul>

					<!-- Menu Social -->
					
					<div class="menu_social_container menu_mm">
						<ul class="menu_social menu_mm">
							<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-pinterest"></i></a></li>
							<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
							<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-instagram"></i></a></li>
							<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-twitter"></i></a></li>
						</ul>
					</div>

					<!--div class="menu_copyright menu_mm">Colorlib All rights reserved</div-->
				</div>

			</div>

		</div>
	

	

	<!-- Home -->
	<div class="home">

		<!-- Hero Slider -->
		<!-- <div class="hero_slider_container">
			<div class="hero_slider">
				<div class="hero_slide">
						<div class="hero_slide_background" style="background-image:url(assets/img/carrousel/carrousel_1.jpg);"></div>
						<div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
							<div class="hero_slide_content text-center" style="min-width: 60%">
								<h1 style="font-size: 35px;" data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut">Hola</h1>
							</div>
						</div>
				</div>


			</div>
		</div> -->
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(assets/img/carrousel/carrousel_1.jpg)"></div>
		</div>
		<div class="home_content">
			<h1>The News</h1>
		</div>


	</div>
</div>



	

</body>
<footer class="main-footer">
	<div class="pull-right hidden-xs">
	  <b>Version</b> 1.0
	</div>
	<strong>Copyright &copy; 2020 <!--a href="https://adminlte.io"--><?php echo $this->sistema ?></a></strong> Todos los derechos reservados.
</footer>

<script type="text/javascript">
    function alerta(title,body,type){
        "use strict";
         // toat popup js
         $.toast({
             heading: title,
             text: body,
             position: 'top-right',
             loaderBg: '#fff',
             icon: type,
             hideAfter: 1700,
             stack: 6
         })
    }
</script>

</html>