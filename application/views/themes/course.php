<?php /*$this->nombre_empresa = 'Favalu';
	  $this->metodo = 'Metodo';
	  $this->controller = 'Controlador';*/
	$imagenes_web = array(
		'logo_cabecera' => 'assets/img/cti_logo.png', 
		'logo_pie' => 'assets/img/logo.png',
		'fondo' => array( 'inicio' => 'images/unsm.jpg' , 
						  'nosotros' => 'images/unsm-tarapoto-cpu-postulantes.jpg' , 
						  'contacto' => 'images/unsm-computo.jpg'  ),
	);

	$informacion_contacto = array(
		'correo' =>  array( 'ctiunsm@gmail.com' ),
		'telefono' => array( '042 -480142',  '955 941 992' , '944 929 637' ),
		'telefono_principal' => '955 941 992',
		'fanpage' => array( 'www.facebook.com/cti.unsm'),
		'direccion' => array( 'Jr. Orellana 575 - Tarapoto'),
	);

  	$modulos_web  = array(  array('Inicio', base_url('web/index') , array()),
					array('Nosotros', base_url('web/nosotros') , array()),
					//array('Talleres', 'talleres.php'),
					array('Cursos', base_url('web/cursos') , array('Informatica Basica','Excel Avanzado')),
					//array('Aliados', 'Aliados.php'),
					array('Contacto', base_url('web/contacto') , array())
				);

  	$sliders_web  = array(  
							array('img' => 'assets/img/carrousel/carrousel_1.jpg',
								  'texto' => '<span> II  SEMINARIO INTERNACIONAL <br> "Aplicabilidad de las TIC\'S en la gestión del conocimiento"</span> ' ),
							array('img' => 'assets/img/carrousel/carrousel_2.jpg',
								  'texto' => "")
					);
  	$redes_sociales  = array(  
	 	'facebook' => array('icono' => 'fab fa-facebook-f', 'link' => 'https://www.facebook.com/cti.unsm' ),
	 	'pinterest' => array('icono' => 'fab fa-pinterest', 'link' => 'https://www.pinterest.es/ctiunsm/' ),
	 	// 'linkedin-in' => array('icono' => 'fab fa-linkedin-in', 'link' => 'https://www.facebook.com/cti.unsm' ),
	 	'instagram' => array('icono' => 'fab fa-instagram', 'link' => 'https://www.instagram.com/cti_unsm/?hl=es-la' ),
	 	'twitter' => array('icono' => 'fab fa-twitter', 'link' => 'https://twitter.com/CtiUnsm' )
					   );

  	$Web_seccion = (trim($this->controller) != '')? $this->controller: 'Bienvenido';
  	$Web_personalizado_home = empty($this->Web_personalizado_home)? 0: 1;

?>
<style type="text/css">
	
	.main_nav_list ul.main_nav_sub_item {
		position:absolute;
		padding: 10px 15px; /*0;*/
		/*margin-top:10px;*/
		/*left:0;*/
		display:none; /* hides sublists */
		background: #349443e8;

	}

	.main_nav_list li:hover ul {display:block;} 
	/*.main_nav_list li:hover {height: 50px;}*/ 

	.main_nav_list ul.main_nav_sub_item li {		
		/*line-height:35px;*/
		padding-bottom: 25px;
	}

	.main_nav_list ul.main_nav_sub_item li a {		
		color: white;
	}

	.main_nav_list ul.main_nav_sub_item li:hover a {		
		color: #349443;
		background: white;
	}

</style>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title> <?php echo $this->pagina_web; ?> </title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="Course Project">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/themes/course/styles/bootstrap4/bootstrap.min.css') ?>">
		<link href="<?php echo base_url('assets/themes/course/plugins/fontawesome-free-5.0.1/css/fontawesome-all.css') ?>" rel="stylesheet" type="text/css">


		<?php
		/** -- Copy from here -- */	
		if(!empty($meta))
		foreach($meta as $name=>$content){
			echo "\n\t\t";
			?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
				 }
		echo "\n";

		if(!empty($canonical))
		{
			echo "\n\t\t";
			?><link rel="canonical" href="<?php echo $canonical?>" /><?php

		}
		echo "\n\t";

		foreach($css as $file){
		 	echo "\n\t\t";
			?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
		} echo "\n\t";

		foreach($js as $file){
				echo "\n\t\t";
				?><script src="<?php echo $file; ?>"></script><?php
		} echo "\n\t";

		/** -- to here -- */ ?>
	</head>
<body>

<div class="super_container">

	<!-- Header -->

	<header class="header d-flex flex-row">
		<div class="header_content d-flex flex-row align-items-center">
			<!-- Logo -->
			<div class="logo_container">
				<div class="logo">
					<img src=" <?= base_url($imagenes_web['logo_cabecera']) ?>" alt="logo_principal" style="width: 120px;height: 54px">
					<!--span>course</span-->
				</div>
			</div>

			<!-- Main Navigation -->
			<nav class="main_nav_container">
				<div class="main_nav">
					<ul class="main_nav_list">

						<? foreach ($modulos_web as $key => $val) : ?>
							<li class='main_nav_item'>
								<a href='<?= $val[1] ?>'><?= $val[0] ?></a>

								<?php if( count($val[2]) > 0 ) { ?>
									<ul class="main_nav_sub_item">

								<? foreach ($val[2] as $skey => $sval) : ?>	
									<li><a href="#"><?= $sval ?></a></li>
								<? endforeach; ?>

									</ul>
								<? }; //endif ?>

							</li>
						<? endforeach; ?>
						
					</ul>
				</div>
			</nav>
		</div>
		<div class="header_side d-flex flex-row justify-content-center align-items-center">
			<img src="<?= base_url('assets/img/iconos/phone-call.svg') ?>" alt="">
			<span><?= $informacion_contacto['telefono_principal'] ?></span>
		</div>

		<!-- Hamburger -->
		<div class="hamburger_container">
			<i class="fas fa-bars trans_200"></i>
		</div>

	</header>
	
	<!-- Menu -->
	<div class="menu_container menu_mm">

		<!-- Menu Close Button -->
		<div class="menu_close_container">
			<div class="menu_close"></div>
		</div>

		<!-- Menu Items -->
		<div class="menu_inner menu_mm">
			<div class="menu menu_mm">
				<ul class="menu_list menu_mm">

					<? foreach ($modulos_web as $key => $val) : ?>
						<li class='menu_item menu_mm'><a href='<?= $val[1] ?>'><?= $val[0] ?></a></li>
					<? endforeach; ?>
				</ul>

				<!-- Menu Social -->
				
				<div class="menu_social_container menu_mm">
					<ul class="menu_social menu_mm">
						<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-pinterest"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-instagram"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-facebook-f"></i></a></li>
						<li class="menu_social_item menu_mm"><a href="#"><i class="fab fa-twitter"></i></a></li>
					</ul>
				</div>

				<!--div class="menu_copyright menu_mm">Colorlib All rights reserved</div-->
			</div>

		</div>

	</div>
	
	
	<?php if(!$this->Web_personalizado_home) { ?>
	<!-- Home -->
	<div class="home">
		<div class="home_background_container prlx_parent">
			<div class="home_background prlx" style="background-image:url(assets/img/carrousel/carrousel_1.jpg)"></div>
		</div>
		<div class="home_content">
			<h1> <?php echo $Web_seccion;?> </h1>
		</div>
	</div>

	<?php } ?>


	<?php echo $output;?>

	<!-- Footer -->

	<footer class="footer">
		<div class="container">
			
			<!-- Newsletter -->

			<div class="newsletter">
				<div class="row">
					<div class="col">
						<div class="section_title text-center">
							<h1>Centro en Tecnologías de Información </h1>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="col text-center">
						<div class="newsletter_form_container mx-auto">
							<form >
								<div class="newsletter_form d-flex flex-md-row flex-column flex-xs-column align-items-center justify-content-center">
									<input id="dni_solicita" class="newsletter_email"  placeholder="Escribe lo que buscas ">
									<button id="newsletter_submit" class="newsletter_submit_btn trans_300" onclick="select_taller()" >Buscar</button>
								</div>
							</form>
						</div>
					</div>
				</div>

			</di>

			<!-- Footer Content -->

			<div class="footer_content">
				<div class="row">

					<!-- Footer Column - About -->
					<div class="col-lg-3 footer_col">

						<!-- Logo -->
						<div class="logo_container">
							<div class="logo">
								<img src="<?= base_url($imagenes_web['logo_pie']) ?>" alt="">
							</div>
						</div>

						<p class="footer_about_text">Centro en Tecnologías de Información, 
						Somos tu llave para triunfar. 
						No te quedes atrás y sé el mejor.</p>

					</div>

					<!-- Footer Column - Menu -->

					<div class="col-lg-3 footer_col">
						<div class="footer_column_title">Menu</div>
						<div class="footer_column_content">
							<ul>

								<? foreach ($modulos_web as $key => $val) : ?>
									<li class='footer_list_item'><a href='<?= $val[1] ?>'><?= $val[0] ?></a></li>
								<? endforeach; ?>

							</ul>
						</div>
					</div>

					<!-- Footer Column - Usefull Links -->

					<div class="col-lg-3 footer_col">
						<div class="footer_column_title">Enlaces</div>
						<div class="footer_column_content">
							<ul>
								<li class="footer_list_item"><a href="https://unsm.edu.pe">Pagina web de UNSM</a></li>
								<li class="footer_list_item"><a href="http://cpu.unsm.edu.pe/">Pagina web de CPU - UNSM</a></li>
								<li class="footer_list_item"><a href="https://unsm.edu.pe/centros-de-produccion/idiomas/">Centro de Idiomas</a></li>
								<li class="footer_list_item"><a href="https://unsm.edu.pe/investigacion/">Investigación y desarrollo</a></li>
								<!-- <li class="footer_list_item"><a href="#">Tuitions</a></li> -->
							</ul>
						</div>
					</div>

					<!-- Footer Column - Contact -->

					<div class="col-lg-3 footer_col">
						<div class="footer_column_title">Informes</div>
						<div class="footer_column_content">
							<ul>
								
								<? foreach ($informacion_contacto['direccion'] as $key => $value) : ?>
								<li class="footer_contact_item">
									<div class="footer_contact_icon">
										<img src="<?=base_url('assets/themes/course/images/placeholder.svg')?>" alt="https://www.flaticon.com/authors/lucy-g">
									</div>									
									<?= $value ?>
								</li>
								<? endforeach; ?>

								
								<li class="footer_contact_item">
									<div class="footer_contact_icon">
										<img src="<?=base_url('assets/themes/course/images/smartphone.svg')?>" alt="https://www.flaticon.com/authors/lucy-g">
									</div>	
									<? foreach ($informacion_contacto['telefono'] as $key => $value) : ?>								
									<?= '('.$value .') '?>

									<? endforeach; ?>
								</li>
								

								<? foreach ($informacion_contacto['correo'] as $key => $value) : ?>
								<li class="footer_contact_item">
									<div class="footer_contact_icon">
										<img src="<?=base_url('assets/themes/course/images/envelope.svg')?>" alt="https://www.flaticon.com/authors/lucy-g">
									</div>									
									<?= $value ?>
								</li>
								<? endforeach; ?>

							</ul>
						</div>
					</div>

				</div>
			</div>

			<!-- Footer Copyright -->
			<div class="footer_bar d-flex flex-column flex-sm-row align-items-center">
				<div class="footer_copyright">
					<span><!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
				Copyright &copy; <script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <?php echo $this->sistema ?> - FISI, UNSM-T <!-- and  <a href="https://colorlib.com" target="_blank">Colorlib --></a>

				<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></span>
				</div>
				<div class="footer_social ml-sm-auto">
					<ul class="menu_social">
						<? foreach ($redes_sociales as $key => $val) : ?>
						 	<li class="menu_social_item"><a href="<?= $val['link'] ?>"><i class='<?= $val['icono'] ?>'></i></a></li>
						<? endforeach; ?>	
					</ul>
				</div>
			</div>

		</div>
	</footer>
</div>

<script src="<?php echo base_url('assets/themes/course/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/styles/bootstrap4/popper.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/styles/bootstrap4/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/TweenMax.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/TimelineMax.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/scrollmagic/ScrollMagic.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/animation.gsap.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/ScrollToPlugin.min.js')?>"></script>

<script src="<?php echo base_url('assets/themes/course/plugins/OwlCarousel2-2.2.1/owl.carousel.js')?> "></script>
<script src="<?php echo base_url('assets/themes/course/plugins/scrollTo/jquery.scrollTo.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/easing/easing.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/js/custom.js')?>"></script>

</body>
</html>