<?php 

  
?>


  <div class="home">

    <!-- Hero Slider -->
    <div class="hero_slider_container">
      <div class="hero_slider owl-carousel">
        

        <? foreach ($sliders_web as $key => $value) { ?>
        <!-- Hero Slide -->
        <div class="hero_slide">
          <div class="hero_slide_background" style="background-image:url( <?=base_url($value['img'])?> )"></div>
          <div class="hero_slide_container d-flex flex-column align-items-center justify-content-center">
            <div class="hero_slide_content text-center">
              <h2 data-animation-in="fadeInUp" data-animation-out="animate-out fadeOut"> <?=$value['texto']?> </h2>
            </div>
          </div>
        </div>
          
        <? } ?>

      </div>

      <div class="hero_slider_left hero_slider_nav trans_200"><span class="trans_200">prev</span></div>
      <div class="hero_slider_right hero_slider_nav trans_200"><span class="trans_200">next</span></div>
    </div>

  </div>

  <div class="hero_boxes">
    <div class="hero_boxes_inner">
      <div class="container">
        <div class="row">

          <? foreach ($sliders_link_web as $key => $value) { ?>
            <div class="col-lg-4 hero_box_col" onclick="window.location='<?=$value['url']?>';">
              <div class="hero_box d-flex flex-row align-items-center justify-content-start">
                <img src="<?=base_url($value['img'])?>" class="svg" alt="">
                <div class="hero_box_content">
                  <h2 class="hero_box_title"><?=$value['texto']?></h2>
                  <a href="<?=$value['url']?>" class="hero_box_link">ver más</a>
                </div>
              </div>
            </div>
          <? } ?>  

        </div>
      </div>
    </div>
  </div>

  <div class="popular page_section">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title text-center">
            <h1>Nuestros Servicios</h1>
          </div>
        </div>
      </div>

      <div class="row course_boxes">
        
        <!-- Popular Course Item -->
        <? foreach ($cti_servicios_web as $key => $val) : ?>       

        <div class="col-lg-4 course_box">
          <div class="card">
            <img class="card-img-top" src="<?= base_url($val['img']) ?>" style="max-height: 185px;" alt="https://www.facebook.com/cti.unsm">
            <div class="card-body text-center">
              <div class="card-title"><a href="talleres.php"><?=$val['card-title']?></a></div>
              <div class="card-text"><?=$val['card-text']?></div>
            </div>

            <? if ( $val['course_author_name'] != '' ):?>
            <div class="price_box d-flex flex-row align-items-center">
              <? if ( $val['course_author_image'] != '' ):?>
                <div class="course_author_image">
                  <img src="<?=$val['course_author_image']?>" alt="https://www.facebook.com/cti.unsm">
                </div>
              <?  endif; ?>

              <div class="course_author_name"><?=$val['course_author_name']?></div>
              <div class="course_price d-flex flex-column align-items-center justify-content-center"><span><?=$val['course_price']?></span></div>
            </div>

            <? endif; ?>
          </div>
        </div>

        <? endforeach; ?>

      </div>
    </div>    
  </div>

  <!-- Register -->

  <div class="register">

    <div class="container-fluid">
      
      <div class="row row-eq-height">
        <div class="col-lg-6 nopadding">
          
          <!-- Register -->
          <div class="register_section d-flex flex-column align-items-center justify-content-center">
            <div class="register_content text-center">
              <h1 class="register_title">Ven y capacítate en  CTI UNSM <span> Sé el mejor !</span> </h1>
              <p class="register_text">El Centro en Tecnología de Información de la Universidad Nacional de San Martin (CTI UNSM), está facultado en el desarrollo de competencias informáticas para profesionales  a demanda del mercado laboral. 

              Contamos con los cursos mas avanzados y solicitados del mercado, dictados por nuestro staff de docentes altamente calificados y profesionales.</p>
              <div class="button button_1 register_button mx-auto trans_200"><a href="#">Inscribirse</a></div>
            </div>
          </div>

        </div>

        <div class="col-lg-6 nopadding">
          
          <!-- Search -->

          <div class="search_section d-flex flex-column align-items-center justify-content-center">
            <div class="search_background" style="background-image:url(assets/themes/course/images/search_background.jpg);"></div>
            <div class="search_content text-center">
              
              <!-- <h1 class="search_title">Search for your course</h1> -->
              <div align="center" class="embed-responsive embed-responsive-16by9">
                  <video autoplay loop controls class="embed-responsive-item">
                      <source src="<?= base_url('assets/video/video_calidad_alta.mp4')?>" type="video/mp4">
                  </video>
              </div>

              <hr>

              <div class="icon_boxes_container" >
              <div class="col-lg-12 icon_box text-left d-flex flex-column align-items-start justify-content-start">
                <!-- <div class="icon_container d-flex flex-column justify-content-end">
                  <img src="<?= base_url('assets/themes/course/images/earth-globe.svg')?> " alt="">
                </div> -->
                <h3 style="color: black">PAGOS DE CURSOS</h3>

                <p style="color: black"> Se debe realizar los pagos solo en: 
                <br> Banco de la Nación en la Cta.: 0000215020-0202050
                <br> Conceptos de Pago: 201-433. <p>

                <h4 style="color: black">Finalmente, guardar el recibo para presentar en oficina. </h4>
              </div>
              </div>

            </div> 
          </div>

        </div>
      </div>
    </div>
  </div>


  

<!-- <script src="<?php echo base_url('assets/themes/course/js/jquery-3.2.1.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/styles/bootstrap4/popper.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/styles/bootstrap4/bootstrap.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/TweenMax.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/TimelineMax.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/scrollmagic/ScrollMagic.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/animation.gsap.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/greensock/ScrollToPlugin.min.js')?>"></script>

<script src="<?php echo base_url('assets/themes/course/plugins/OwlCarousel2-2.2.1/owl.carousel.js')?> "></script>
<script src="<?php echo base_url('assets/themes/course/plugins/scrollTo/jquery.scrollTo.min.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/plugins/easing/easing.js')?>"></script>
<script src="<?php echo base_url('assets/themes/course/js/custom.js')?>"></script> -->



