<?php 

$fondo =   base_url('assets/themes/course/images/search_background.jpg');

$cursos_lista = array( 
                      array('img'=>'assets/themes/course/images/course_1.jpg',
                          'titulo'=>'Informatica basica',
                          'texto'=>'Curso de informatica basica.',
                          'img_doc'=>'assets/themes/course/images/author.jpg',
                          'name_doc'=>'Michael Smith',
                          'cargo_doc'=>'Ing'
                          
                      )
                    );

?>
<!-- Home -->

  <div class="home">
    <div class="home_background_container prlx_parent">
      <div class="home_background prlx" style="background-image:url(<?= $fondo ?>)"></div>
    </div>
    <div class="home_content">
      <h1>Cursos</h1>
    </div>
  </div>

  <!-- Popular -->

  <div class="popular page_section">
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title text-center">
            <h1>Lista de Cursos</h1>
          </div>
        </div>
      </div>

      <div class="row course_boxes">
        
        <?php foreach ($cursos_lista as $key => $value) { ?>
        <!-- Popular Course Item -->
        <div class="col-lg-4 course_box">
          <div class="card">
            <img class="card-img-top" src="<?= base_url($value['img']) ?>" alt="https://unsplash.com/@kellybrito">
            <div class="card-body text-center">
              <div class="card-title"><a href="<?= base_url($value['img']) ?> courses.html"><?= $value['titulo'] ?></a></div>
              <div class="card-text"><?= $value['texto'] ?></div>
            </div>
            <div class="price_box d-flex flex-row align-items-center">
              <div class="course_author_image">
                <img src="<?= base_url($value['img_doc']) ?>" alt="https://unsplash.com/@mehdizadeh">
              </div>
              <div class="course_author_name"><?= $value['name_doc'] ?>, <span><?= $value['cargo_doc'] ?></span></div>
              <div class="course_price d-flex flex-column align-items-center justify-content-center"><span>$29</span></div>
            </div>
          </div>
        </div>

        <?php } //endforeach ?>

      </div>
    </div>    
  </div>