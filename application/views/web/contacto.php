<!-- Home -->

  <div class="home">
    <div class="home_background_container prlx_parent">
      <div class="home_background prlx" style="background-image:url(<?= base_url('assets/themes/course/images/contact_background.jpg') ?>)"></div>
    </div>
    <div class="home_content">
      <h1>Contacto</h1>
    </div>
  </div>

  <!-- Contact -->

  <div class="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-8">
          
          <!-- Contact Form -->
          <div class="contact_form">
            <div class="contact_title">Ponerse en contacto</div>

            <div class="contact_form_container">
              <form action="post">
                <input id="contact_form_name" class="input_field contact_form_name" type="text" placeholder="Name" required="required" data-error="Name is required.">
                <input id="contact_form_email" class="input_field contact_form_email" type="email" placeholder="E-mail" required="required" data-error="Valid email is required.">
                <textarea id="contact_form_message" class="text_field contact_form_message" name="message" placeholder="Message" required="required" data-error="Please, write us a message."></textarea>
                <button id="contact_send_btn" type="button" class="contact_send_btn trans_200" value="Submit">Enviar mensaje</button>
              </form>
            </div>
          </div>
            
        </div>

        <div class="col-lg-4">
          <div class="about">
            <div class="about_title">Unirse a Cursos</div>
            <p class="about_text">Cursos de tecnología en todas las especialidades, para conocimiento profesional o capacitación a equipos de trabajo, de acuerdo a la exigencia del mercado laboral.</p>

            <div class="contact_info">
              <ul>
                <li class="contact_info_item">
                  <div class="contact_info_icon">
                    <img src="images/placeholder.svg" alt="https://www.flaticon.com/authors/lucy-g">
                  </div>
                  Blvd Libertad, 34 m05200 Arévalo
                </li>
                <li class="contact_info_item">
                  <div class="contact_info_icon">
                    <img src="images/smartphone.svg" alt="https://www.flaticon.com/authors/lucy-g">
                  </div>
                  0034 37483 2445 322
                </li>
                <li class="contact_info_item">
                  <div class="contact_info_icon">
                    <img src="images/envelope.svg" alt="https://www.flaticon.com/authors/lucy-g">
                  </div>hello@company.com
                </li>
              </ul>
            </div>

          </div>
        </div>

      </div>

      <!-- Google Map -->

      <div class="row">
        <div class="col">
          <div id="google_map">
            <div class="map_container">
              <div id="map">MAPA GOOGLE</div>
            </div>
          </div>
        </div>
      </div>

    </div>
  </div>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>