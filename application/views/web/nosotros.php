<?php 

  $fondo =   base_url('assets/themes/course/images/search_background.jpg');

  $nosotros_info = array( 
                      array('img'=>'assets/themes/course/images/blackboard.svg',
                          'texto'=>'Al 2021, somos un Centro de Producción de la Universidad Nacional de San Martín-T, líder en el campo de servicios de capacitación en TIC en la amazonia peruana, con tecnologías de tendencias actuales que conlleven a la calidad educativa esperada al servicio de la sociedad.',
                          'titulo'=>'Visión'
                      ),
                      array('img'=>'assets/themes/course/images/books.svg',
                          'texto'=>'Somos un Centro de Producción de la Universidad Nacional de San Martín-T, administrada por la Facultad de Ingeniería de Sistemas e Informática, dedicada a brindar servicios de capacitación en TIC a la comunidad universitaria y público en general; mejorando la competitividad y creatividad de nuestros estudiantes y respondan a las exigencias del mercado, integrando y complementado su formación académica.',
                          'titulo'=>'Misión'
                      )
                    );
?>

<!-- Home -->
<div class="home">
    <div class="home_background_container prlx_parent">
      <div class="home_background prlx" style="background-image:url(<?= $fondo ?>)"></div>
    </div>
    <div class="home_content">
      <h1> <?= $Web_seccion ?> </h1>
    </div>
</div>

<!-- Services -->
  <div class="services page_section">
    
    <div class="container">
      <div class="row">
        <div class="col">
          <div class="section_title text-center">
            <h1>Centro en Tecnologías de Información</h1>
          </div>
          <div>
            <p>La creación del centro de producción de la Facultad de Ingeniería de Sistemas e Informática fue aprobada con resolución N°026-2003-UNSM/CRYG-ANR. Donde se otorga la administración del centro a la FISI.</p>
            <p>El estatuto de la Universidad Nacional de San Martin -Tarapoto, aprobó con resolución N° 005-2016/AUR/NLU del 15.02.2016, con articulo 21° indica: las facultades, para el mejor cumplimiento de sus funciones, (…). Asimismo, Formara comisiones Permanentes y/o Especiales, cuando el Consejo de Facultad lo Requiera, las cuales rendirán cuentas a su consejo del cumplimiento de sus tareas.</p>
            <p>La comisión de Administración del CTI está conformada por los docentes de la FISI, los cuales son encargados de velar por el normal desarrollo del Centro, su mejora continua, proyección a la comunidad y sostenibilidad en el tiempo a través de la oferta de sus servicios hacia la comunidad universitaria y a la población en general.</p>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col">
          <div align="center" class="embed-responsive embed-responsive-16by9" style="max-height: 250px">
              <video autoplay loop class="embed-responsive-item">
                  <source src="<?= base_url('assets/video/video_calidad_alta.mp4')?>" type="video/mp4">
              </video>
          </div>
        </div>
      </div>

      <br>

      <div class="row services_row">

        <? foreach ($nosotros_info as $key => $value) { ?>

        <div class="col-lg-6 service_item text-left d-flex flex-column align-items-start justify-content-start">
          <div class="icon_container d-flex flex-column justify-content-end">
            <img src="<?= base_url($value['img']) ?>" alt="">
          </div>
          <h3><?= $value['titulo'] ?></h3>
          <p><?= $value['texto'] ?>.</p>
        </div>

        <? }  ?>

      </div>
    </div>
  </div>
