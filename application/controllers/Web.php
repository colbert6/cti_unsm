<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends MY_Controller {

	function __construct()
    {
        parent::__construct();
        $this->controller = 'Web';//Siempre define las migagas de pan
    }


    public function index()
    {	
        $this->output->set_template('course');

        /*--Cargando Css--*/
        $this->load->css('assets/themes/course/plugins/OwlCarousel2-2.2.1/owl.carousel.css');
        $this->load->css('assets/themes/course/plugins/OwlCarousel2-2.2.1/owl.theme.default.css');  
        $this->load->css('assets/themes/course/plugins/OwlCarousel2-2.2.1/animate.css');
        $this->load->css('assets/themes/course/styles/main_styles.css');  
        $this->load->css('assets/themes/course/styles/responsive.css'); 


        // Parametros para template
        $this->controller =  'Bienvenidos al  Centro en Tecnología de Información ';
        $this->metodo =  '';      
        $this->Web_personalizado_home = 1;

    	// Parametros para la vista
    	$output = array('title' => 'Principal' ); 
        $output['texto'] =  "Bienvenido";
        $output['sliders_web'] =  array(  
            array('img' => 'assets/img/carrousel/carrousel_1.jpg',
                'texto' => '<span>Centro en Tecnología de Información de la UNSM-T</span>' ),
            array('img' => 'assets/img/carrousel/carrousel_2.jpg',
                'texto' => '<span>Capacítate y desarrolla tus Competencias en tecnología</span>')
        );
        $output['sliders_link_web'] =  array(  
            array('img' => 'assets/themes/course/images/earth-globe.svg',
                'texto' => 'Matricúlate','url' => '' ),
            array('img' => 'assets/themes/course/images/books.svg',
                'texto' => 'Talleres','url' => '' ),
            array('img' => 'assets/themes/course/images/professor.svg',
                'texto' => 'Calendario','url' => '' )
        );

        $output['cti_servicios_web'] =  array(  

              array('img' => 'assets/img/servicios/servicios_4.png',
                    'card-title' => "EXAMEN  DE SUFICIENCIA",
                    'card-text' => "<br>CTI - UNSM,  te facilita el requisito de suficiencia en cursos informáticos, a través de la administración de un examen de suficiencia.",
                    'course_author_image' => "",
                    'course_author_name' => "Más información",
                    'course_price' => "+" ),

              array('img' => 'assets/img/servicios/servicios_5.jpg',
                    'card-title' => "CAPACITACIÓN DE EMPRESAS",
                    'card-text' => "<br> Cursos y programas dictados por docentes especialistas, diseñados a la medida de las necesidades en capacitación informática de su  organización.",
                    'course_author_image' => "",
                    'course_author_name' => "Más información",
                    'course_price' => "+" ),
                    
              array('img' => 'assets/img/servicios/servicios_3.jpg',
                    'card-title' => "CURSOS DEL MES",
                    'card-text' => "<br>Se muestra la lista de los diversos cursos programados para el mes actual y/o proximos.",
                    'course_author_image' => "",
                    'course_author_name' => "Más información",
                    'course_price' => "+" )
        );
      
        $this->load->view('web/index', $output ) ;
    }

    public function nosotros()
    {   
        
        $this->output->set_template('course');

        /*--Cargando Css--*/
        $this->load->css('assets/themes/course/styles/elements_styles.css');  
        $this->load->css('assets/themes/course/styles/elements_responsive.css'); 

        // Parametros para template
        $this->controller =  'Bienvenidos al  Centro en Tecnología de Información ';
        $this->metodo =  '';      
        $this->Web_personalizado_home = 1;

        // Parametros para la vista
        $output = array('title' => 'Principal' ); 
        $output['texto'] =  "Nosotros";
        $output['Web_seccion'] =  "Nosotros";
      
        $this->load->view('web/nosotros', $output ) ;
    }

    public function cursos()
    {   
        
        $this->output->set_template('course');

        /*--Cargando Css--*/
        $this->load->css('assets/themes/course/styles/courses_styles.css');  
        $this->load->css('assets/themes/course/styles/courses_responsive.css'); 

        // Parametros para template
        $this->controller =  'Bienvenidos al  Centro en Tecnología de Información ';
        $this->metodo =  '';      
        $this->Web_personalizado_home = 1;

        // Parametros para la vista
        $output = array('title' => 'Principal' ); 
        $output['texto'] =  "Nosotros";
        $output['Web_seccion'] =  "Nosotros";
      
        $this->load->view('web/cursos', $output ) ;
    }

    public function contacto()
    {   
        
        $this->output->set_template('course');

        /*--Cargando Css--*/
        $this->load->css('assets/themes/course/styles/contact_styles.css');  
        $this->load->css('assets/themes/course/styles/contact_responsive.css'); 

        // Parametros para template
        $this->controller =  'Bienvenidos al  Centro en Tecnología de Información ';
        $this->metodo =  '';      
        $this->Web_personalizado_home = 1;

        // Parametros para la vista
        $output = array('title' => 'Principal' ); 
        $output['texto'] =  "Contacto";
        $output['Web_seccion'] =  "Contacto";
      
        $this->load->view('web/contacto', $output ) ;
    }

    public function blanco()
    {   
        
        $this->output->set_template('course');

        /*--Cargando Css--*/
        $this->load->css('assets/themes/course/styles/news_styles.css');
        $this->load->css('assets/themes/course/styles/news_responsive.css');  

        // Parametros para template
        $this->controller =  'Bienvenidos';
        $this->metodo =  '';      

        // Parametros para la vista
        $output = array('title' => 'Principal' ); 
        $output['texto'] =  "Bienvenido";
      
        $this->load->view('web/blanco', $output ) ;
    }


}
